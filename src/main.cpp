#include <iostream>
#include <string>
#include <vector>
#include <filesystem>

#include "db.hpp"

namespace fs = std::filesystem;

int main()
{
    taggr_db::connection conn("test.db");
    
    std::vector<fs::directory_entry> files;

    auto path = fs::current_path();
    
    for (auto const& entry : fs::recursive_directory_iterator(path)) {
	    if (entry.is_regular_file()) {
            files.push_back(entry);
        }
    }
	
    for (auto const& entry : files) {
        conn.insert_file(entry.path().string());
    }
    return 0;
}
