#include "db.hpp"

#include <string>
#include <vector>

#include <fmt/core.h>

using namespace taggr_db;

connection::connection(const std::string& db_path)
{
    int error = sqlite3_open(db_path.c_str(), &this->sqlite);
    sqlite3_extended_result_codes(this->sqlite, 1);

    if (error != SQLITE_OK) {
        throw std::runtime_error(sqlite3_errmsg(this->sqlite));
    }

    this->init_db();
}

connection::~connection()
{
    sqlite3_close(sqlite);
}


void connection::execute_sql(const std::string& sql)
{
    int error = 0;
    sqlite3_stmt* statement = NULL;

    error = sqlite3_prepare_v2(this->sqlite, sql.c_str(), sql.length(), &statement, NULL);
    if (error != SQLITE_OK) {
        throw std::runtime_error(sqlite3_errmsg(this->sqlite));
    }

    error = sqlite3_step(statement);
    if (error != SQLITE_DONE) {
        throw std::runtime_error(sqlite3_errmsg(this->sqlite));
    }

    sqlite3_finalize(statement);
}

void connection::init_db(void)
{
    int error = 0;

    for (const std::string& sql : SQL_INIT_DB) {
        this->execute_sql(sql);
    }
}

void connection::insert_file(const std::string& path)
{
    const std::string sql = fmt::format("INSERT INTO Files (path) VALUES ('{}')", path);

    sqlite3_stmt* statement = NULL;
    int error = sqlite3_prepare_v2(this->sqlite, sql.c_str(), sql.length(), &statement, NULL);
    if (error != SQLITE_OK) {
        throw std::runtime_error(sqlite3_errmsg(this->sqlite));
    }

    error = sqlite3_step(statement);
    if (error == SQLITE_CONSTRAINT_UNIQUE) {
        fmt::print(stderr, "{} already indexed\n", path);
    }
    else if (error != SQLITE_DONE) {
        throw std::runtime_error(sqlite3_errmsg(this->sqlite));
    }

    sqlite3_finalize(statement);
}
