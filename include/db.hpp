#pragma once

#include <iostream>
#include <string>
#include <vector>

#include <sqlite3.h>


namespace taggr_db
{
    const std::vector<std::string> SQL_INIT_DB{
        "CREATE TABLE IF NOT EXISTS Tags (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name CHAR UNIQUE);",
        "CREATE TABLE IF NOT EXISTS Files(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, path CHAR UNIQUE);",
        "CREATE TABLE IF NOT EXISTS TagFile(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, tag_id INTEGER, file_id INTEGER, \
                                            FOREIGN KEY(tag_id) REFERENCES Tags(id) ON DELETE CASCADE,\
                                            FOREIGN KEY(file_id) REFERENCES Files(id) ON DELETE CASCADE);"
    };


    class connection
    {
    private:
        sqlite3* sqlite;
        void execute_sql(const std::string&);
    
    public:
        connection(const std::string&);
        ~connection();
        void init_db(void);
        void insert_file(const std::string&);
    };
};
